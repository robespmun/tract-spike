import React from 'react';

/**
 * Surrounds the children component with a span having the following classes:
 * - the "draggable" class will always be present
 * - the "dragged" class will be added while dragging and removed when the drag is concluded
 * - the "disabled" class will be present if the disabled prop has been valued with a true
 * All the properties passed to this component will be passed through so yon can continue to use the react standard props like className,onDragStart,onDragEnter,onDragOver,onDragLeave,onDrop.
 **/
export default React.createClass({

    getInitialState() {
        return {
            dragged: false
        }
    },

    onDragStart(){
        this.setState({dragged: true});
        if(this.props.onDragStart){
            this.props.onDragStart();
        }
    },

    onDragEnd(){
        this.setState({dragged: false});
        if(this.props.onDrop){
            this.props.onDrop();
        }
    },

    render(){

        const {children,className,disabled,...rest} = this.props;

        let passedClasses = className || '';

        passedClasses = passedClasses.replace(' dragged','');
        passedClasses = passedClasses.replace(' draggable','');
        passedClasses = passedClasses.replace(' disabled','');

        let newClasses = passedClasses + ' draggable';
        newClasses = newClasses + (this.state.dragged ? ' dragged' : '');
        newClasses = newClasses + (disabled ? ' disabled' : '');

        return <div draggable={!disabled}
            {...rest} /*do not move this assignment down it will break the override mechanism*/
                     className={newClasses}
                     onDragStart={this.onDragStart}
                     onDragEnd={this.onDragEnd}
        >
            {children}
        </div>

    }

});