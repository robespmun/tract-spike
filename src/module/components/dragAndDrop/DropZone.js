import React from 'react';

/**
 * Surrounds the children component with a span that will have the "drag-over" class when dragging inside it.
 * Furthermore it takes care of the HTML 5 drag and drop API inconsistencies between browsers.
 * It takes also care of some unobvious steps you'd need in order to implement a drop zone e.g it preventDefault the dragOver for you.
 * All the properties passed to this component will be passed through so yon can continue to use the react standard props like className,onDragStart,onDragEnter,onDragOver,onDragLeave,onDrop.
 **/
export default React.createClass({

    getInitialState() {
        return {
            counter: 0
        }
    },

    onDragEnter(event){
        if(event.relatedTarget===undefined){ return } //firefox fire the dragEnter twice, in one of them relatedTarget is undefined
        this.setState({counter: this.state.counter+1});
        if(this.props.onDragEnter){
            this.props.onDragEnter();
        }
    },

    onDragOver(event){
        event.preventDefault(); //needed in order to make this element a drop zone (the default action is to cancel the drop operation)
        if(this.props.onDragOver){
            this.props.onDragOver();
        }
    },

    onDragStart(event){
        if(event.dataTransfer.types.length===0){
            event.dataTransfer.setData('text',''); //firefox doesn't fire the onDragLeave if no dataTransfer is present
        }
        if(this.props.onDragStart){
            this.props.onDragStart();
        }
    },

    onDragLeave(){
        if(this.props.onDragLeave){
            this.props.onDragLeave();
        }
        this.setState({counter: this.state.counter-1});
    },

    onDrop(){
        this.setState({counter: 0});
        if(this.props.onDrop){
            this.props.onDrop();
        }
    },

    render(){

        const {children,className,...rest} = this.props;

        let passedClasses = className || '';
        passedClasses = passedClasses.replace(' drag-over','');
        const newClasses = passedClasses + (this.state.counter > 0 ? ' drag-over' : '');

        return <div
            {...rest} /*do not move this assignment down it will break the override mechanism*/
            className={newClasses}
            onDragStart={this.onDragStart}
            onDragEnter={this.onDragEnter}
            onDragOver={this.onDragOver}
            onDragLeave={this.onDragLeave}
            onDrop={this.onDrop}
        >
            {children}
        </div>

    }

});