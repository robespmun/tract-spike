import React from 'react';
import Draggable from '../dragAndDrop/Draggable';

export default React.createClass({

    onChange(event){
        this.props.onChange(this.props.data.uuid, {...this.props.data, text: event.target.value});
    },

    onDrag(){
        this.props.onDrag(this.props.data.uuid);
    },

    render(){
        const {data} = this.props;
        return <Draggable onDragStart={this.onDrag}>
            <textarea name="text" onChange={this.onChange} value={data.text}/>
        </Draggable>
    }

})
