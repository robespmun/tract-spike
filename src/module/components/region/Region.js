import React from 'react';
import DropZone from '../dragAndDrop/DropZone';
import './region.scss';

export default React.createClass({

    onAdd(){
      this.props.onAdd(this.props.data.uuid);
    },

    onDrop(){
        this.props.onDrop(this.props.data.uuid);
    },

    render() {
        const {data, children} = this.props;
        return <DropZone onDrop={this.onDrop}>
            <div className="region">
                <h1>{data.title}</h1>
                <button onClick={this.onAdd}>Add</button>
                {children}
            </div>
        </DropZone>
    }

})
