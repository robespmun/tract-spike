import React from 'react';
import Layout from './layout/Layout';
import Region from './region/Region';
import TextWidget from './textWidget/TextWidget';
import DragState from './dragAndDrop/DragState';

export default React.createClass({

    idToComponent: {
        region: Region,
        textWidget: TextWidget
    },

    getInitialState(){
        return {
            data: [{
                id: 'region',
                data: {uuid: 1, title: 'Title 1'},
                children: [{
                    id: 'textWidget',
                    data: {uuid: 2, text: 'some text'},
                    children: []
                }]
            }, {
                id: 'region',
                data: {uuid: 3, title: 'Title 1'},
                children: []
            }, {
                id: 'region',
                data: {uuid: 4, title: 'Title 1'},
                children: []
            }]
        }
    },

    getByUUID(children, uuid){
        let result = undefined;
        children.forEach((child)=> {
            if (child.data.uuid === uuid) {
                result = child;
            }
            if (!result) {
                result = this.getByUUID(child.children, uuid);
            }
        });
        return result;
    },

    removeByUUID(children, uuid){
        let result = undefined;
        children.forEach((child, i)=> {
            if (child.data.uuid === uuid) {
                result = child;
                children.splice(i, 1);
            }
            if (!result) {
                result = this.removeByUUID(child.children, uuid);
            }
        });
        return result;
    },

    onDrag(uuid){
        const draggedChild = this.getByUUID(this.state.data, uuid);
        DragState.draggedItem = draggedChild;
    },

    onDrop(uuid){
        const droppedChild = DragState.draggedItem;
        this.removeByUUID(this.state.data, droppedChild.data.uuid);
        let child = this.getByUUID(this.state.data, uuid);
        child.children.push(droppedChild);
        this.setState(this.state);
    },

    onChange(uuid, newData){
        let child = this.getByUUID(this.state.data, uuid);
        child.data = newData;
        this.setState(this.state);
    },

    onAdd(uuid){
        let child = this.getByUUID(this.state.data, uuid);
        child.children.push({
            id: 'textWidget',
            data: {uuid: new Date().getTime(), text: ''},
            children: []
        });
        this.setState(this.state);
    },

    createChild(child){
        return React.createElement(this.idToComponent[child.id], {
            data: child.data,
            key: child.data.uuid,
            onAdd: this.onAdd,
            onDrag: this.onDrag,
            onDrop: this.onDrop,
            onChange: this.onChange,
            children: child.children.map((child)=> {
                return this.createChild(child);
            })
        });
    },

    render() {
        const {data} = this.state;
        return <Layout>
            {data.map((child)=> {
                return this.createChild(child);
            })}
        </Layout>
    }

})
