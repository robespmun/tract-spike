import React from 'react';
import './layout.scss';

export default React.createClass({

    render() {
        return <div className="layout-container">
            <div className="sidebar">Sidebar</div>
            <div className="main-zone">{this.props.children}</div>
        </div>
    }
})
