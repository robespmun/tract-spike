import React from 'react';
import ReactDOM from 'react-dom';
import Tract from './module/components/Tract';

ReactDOM.render(
    <Tract />,
    document.getElementById('root')
);
